# Memory Fire (aka DRAM Eater)

https://ai18n.tk/memory-fire

# 使い方

## 投稿する

テキストボックスに文字列を入力して、Postボタンを押すと投稿できます。

LockチェックボックスをONにした状態で投稿すると、ロックされた投稿になります。ロックされた投稿は、普通の方法では見ることができません。

## 投稿を見る

Deleteリンクを押すと、投稿を削除することができます。これは、他人の投稿を消すことも可能です。

Unlockチェックボックスをオンにすると、ロックされた投稿の中身を見ることができます。ロックされた投稿が複数あるときは、それらをすべて見ることができます。

## コンピューターの浪費

以下の操作を行うとき、あなたのコンピューターの能力を浪費します。

* 投稿するとき。通常の投稿とロックされたと投稿では、浪費されるコストは同じです。
* 投稿を削除するとき。
* ロックされた投稿を見るとき。(浪費する計算量は他の操作の4倍です。)

浪費されるコンピューターの能力は、計算時間が20秒くらい、メモリスペースが8メガバイトくらいです。

# Install

## Depends on

* build-essential
* apache2
* certbot
* python3-certbot-apache
* libcrypto++-dev

## Install steps

```
sudo ln -s *code*/client /var/www/html/memory-fire  
cd *code*/server  
make clean  
make  
sudo make install  
sudo make initialize  
sudo make activate-cgi
```
