async function drameater (initializer, start, step, returnProgreee) {

	/* Functions */

	function advance (input, instruction) {
		const shift = parseInt (instruction.substring (0, 4), 16) % 60
		let after_shift_array = new Array (60)
		for (let cn = 0; cn < input.length; cn ++) {
			after_shift_array[(cn + shift) % 60] = input[cn]
		}
		const after_shift = after_shift_array.join ('')

		let after_add = new String
		for (let cn = 0; cn < 15; cn ++) {
			after_add +=
				((parseInt (after_shift.substring (cn * 4, cn * 4 + 4), 16) +
				parseInt (instruction.substring (cn * 4 + 4, cn * 4 + 8), 16)) % 0x10000)
				.toString (16)
				.padStart (4, '0')
		}

		// console.log (input + ' ' + after_add)
		return after_add
	}

	/* Main */

	let hash = initializer
	let hashArray = []
	for (let cn = 0; cn < step; cn ++) {
		if (cn % (step / 0x400) == 0) {
			returnProgreee (cn / step)
		}
		hashArray.push (hash)
		hash = await strsha256 (hash)
	}

	let state = start

	for (let cn = 0; cn < step; cn ++) {
		state = advance (state, hashArray[step - cn - 1])
	}

	return state

} /* async function drameater (step, goal, digits, fill) { */

