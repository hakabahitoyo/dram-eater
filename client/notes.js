
let previous_text = ''
let privious_locked = false


window.addEventListener ('load', function () {
document.getElementById ('button-post').addEventListener ('click', function () {
	let text = document.getElementById ('input-post').value
	let locked = document.getElementById ('checkbox-lock').checked

	if (! text) {
		alert ('What?')
	} else if (text === previous_text && locked === previous_locked) {
		alert ('Not again.')
	} else {
		previous_text = text
		previous_locked = locked

		const request_1 = new XMLHttpRequest ()
		request_1.open ('POST', '/cgi-bin/dram-eater-notes-create-quiz.cgi')
		request_1.addEventListener ('load', async function () {
			if (request_1.status == 200) {
				const progressesElement = document.getElementById ('placeholder-progresses')
				const progressElement = document.createElement ('span')
				progressElement.innerText = '0.000 '
				progressesElement.appendChild (progressElement)

				const responseText = request_1.responseText
				const responseJson = JSON.parse (responseText)
				const sessionId = responseJson.sessionId

				const answer = await drameater (
					responseJson.initializer,
					responseJson.start,
					responseJson.step,
					function (progress) {
						progressElement.innerText = progress.toFixed (3) + ' '
					}
				)

				progressElement.innerText = '1.000 '
				
				const request_2 = new XMLHttpRequest ()
				request_2.open ('POST', '/cgi-bin/dram-eater-notes-post.cgi')
				request_2.addEventListener ('load', function () {
					progressesElement.removeChild (progressElement)
					if (request_2.status == 200) {
						getAndShowPosts ()
					}
				}, false)
				request_2.send (JSON.stringify ({
					'sessionId': sessionId,
					'answer': answer,
					'text': text,
					'locked': locked
				}))
			}
		}, false)
		request_1.send (JSON.stringify ({'scale': '1'}))
	}
}, false) /* document.getElementById ('button-post').addEventListener ('input', function () { */
}, false) /* window.addEventListener ('load', function () { */


window.addEventListener ('load', function () {
	getAndShowPosts ()
}, false)


function getAndShowPosts () {
	if (document.getElementById ('checkbox-unlock').checked) {
		const request_1 = new XMLHttpRequest ()
		request_1.open ('POST', '/cgi-bin/dram-eater-notes-create-quiz.cgi')
		request_1.addEventListener ('load', async function () {
			if (request_1.status == 200) {
				const progressesElement = document.getElementById ('placeholder-progresses')
				const progressElement = document.createElement ('span')
				progressElement.innerText = '0.000 '
				progressesElement.appendChild (progressElement)

				const responseText = request_1.responseText
				const responseJson = JSON.parse (responseText)
				const sessionId = responseJson.sessionId

				const answer = await drameater (
					responseJson.initializer,
					responseJson.start,
					responseJson.step,
					function (progress) {
						progressElement.innerText = progress.toFixed (3) + ' '
					}
				)

				progressElement.innerText = '1.000 '
				
				const request_2 = new XMLHttpRequest ()
				request_2.open ('POST', '/cgi-bin/dram-eater-notes-get-locked.cgi')
				request_2.addEventListener ('load', function () {
					progressesElement.removeChild (progressElement)
					if (request_2.status == 200) {
						const cjson = request_2.responseText;
						showPosts (cjson)
					}
				}, false)
				request_2.send (JSON.stringify ({
					'sessionId': sessionId,
					'answer': answer
				}))
			}
		}, false)
		request_1.send (JSON.stringify ({'scale': '4'}))
	} else {
		const request_1 = new XMLHttpRequest ()
		request_1.open ('GET', '/cgi-bin/dram-eater-notes-view.cgi')
		request_1.addEventListener ('load', function () {
			if (request_1.status == 200) {
				const cjson = request_1.responseText;
				showPosts (cjson)
			}
		}, false)
		request_1.send ()
	}
}


function scopeToEmoji (scope) {
	const emoji = 
		{
			'Public': '🌐',
			'Locked': '🔒',
			'Unlocked': '🔓'
		}[scope]
	return emoji
}


function showPosts (cjson) {
	const placeholder = document.getElementById ('placeholder-notes');
	while (placeholder.firstChild) {
		placeholder.removeChild (placeholder.firstChild)
	}

	const jsons = cjson.split ("\n")
	for (let cn = 0; cn < jsons.length; cn ++) {
		try {
			const json = jsons[jsons.length - cn - 1]
			const post = JSON.parse (json)
			const p = document.createElement ('p')
			p.setAttribute ('id', 'anchor-' + post.id)
			if (post.text.startsWith ('https://gitlab.com/') &&
				(post.text.endsWith ('.png') ||
					post.text.endsWith ('.jpeg') ||
					post.text.endsWith ('.jpg'))) {
				const img = document.createElement ('img')
				img.setAttribute ('src', post.text)
				img.setAttribute ('style', 'width: 512px; max-width: 100%; max-height: 384px; object-fit: contain;')
				p.appendChild (img)
				p.appendChild (document.createElement ('br'))
			}
			if (post.text.startsWith ('http://') || post.text.startsWith ('https://')) {
				const aLink = document.createElement ('a')
				aLink.innerText = post.text
				aLink.setAttribute ('href', post.text)
				aLink.setAttribute ('target', '_blank')
				p.appendChild (aLink)
			} else {
				const text = document.createTextNode (post.text)
				p.appendChild (text)
			}
			const br = document.createElement ('br')
			p.appendChild (br)
			const small = document.createElement ('small')
			small.appendChild (document.createTextNode ('('))
			small.appendChild (document.createTextNode (showTime (post.time)))
			small.appendChild (document.createTextNode (' '))
			small.appendChild (document.createTextNode (scopeToEmoji (post.scope)))
			small.appendChild (document.createTextNode (' '))
			const aDelete = document.createElement ('a')
			aDelete.innerText = 'Delete'
			aDelete.setAttribute ('href', "javascript:deletePost('" + post.id + "')")
			small.appendChild (aDelete)
			small.appendChild (document.createTextNode (')'))
			p.appendChild (small)
			placeholder.appendChild (p)
		} catch (error) {
			console.log (error)
		}
	}
}


function showTime (seconds_from_unix_epoch) {
	const date = new Date (seconds_from_unix_epoch * 1000)
	format = date.getFullYear ().toFixed ().padStart (4, '0') + '-' +
		(date.getMonth () + 1).toFixed ().padStart (2, '0') + '-' +
		date.getDate ().toFixed ().padStart (2, '0') + ' ' +
		date.getHours ().toFixed ().padStart (2, '0') + ':' +
		date.getMinutes ().toFixed ().padStart (2, '0')
	return format
}


function deletePost (id) {
	const request_1 = new XMLHttpRequest ()
	request_1.open ('POST', '/cgi-bin/dram-eater-notes-create-quiz.cgi')
	request_1.addEventListener ('load', async function () {
		if (request_1.status == 200) {
			const progressesElement = document.getElementById ('placeholder-progresses')
			const progressElement = document.createElement ('span')
			progressElement.innerText = '0.000 '
			progressesElement.appendChild (progressElement)

			const responseText = request_1.responseText
			const responseJson = JSON.parse (responseText)
			const sessionId = responseJson.sessionId

			const answer = await drameater (
				responseJson.initializer,
				responseJson.start,
				responseJson.step,
				function (progress) {
					progressElement.innerText = progress.toFixed (3) + ' '
				}
			)

			progressElement.innerText = '1.000 '
			
			const request_2 = new XMLHttpRequest ()
			request_2.open ('POST', '/cgi-bin/dram-eater-notes-delete.cgi')
			request_2.addEventListener ('load', function () {
				progressesElement.removeChild (progressElement)
				if (request_2.status == 200) {
					getAndShowPosts ()
				}
			}, false)
			request_2.send (JSON.stringify ({
				'sessionId': sessionId,
				'answer': answer,
				'id': id
			}))
		}
	}, false)
	request_1.send (JSON.stringify ({'scale': '1'}))
}


window.addEventListener ('load', function () {
	window.setInterval (function () {
		if (! document.getElementById ('checkbox-unlock').checked) {
			getAndShowPosts ()
		}
	}, 10 * 1000)
}, false)


window.addEventListener ('load', function () {
	document.getElementById ('checkbox-unlock').addEventListener ('change', function () {
		getAndShowPosts ()
	}, false)
}, false)

