#include <iostream>
#include <cstdlib>
#include <sstream>
#include <cassert>

#include "picojson.h"
#include "drameater.h"

using namespace std;


int main (int argc, char ** argv)
{
	auto method_c = getenv ("REQUEST_METHOD");
	string method {method_c == nullptr? "": method_c};

	if (method != string {"POST"}) {
		cout << drameater::make_cgi_header (string {"500"});
		cout << "Invalid HTTP method: " << method << endl;
		exit (0);
	}

	auto buffer = static_cast <char *> (malloc (4096));
	assert (buffer != nullptr);

	string input_string;

	for (; ; ) {
		auto result = fgets (buffer, 4096, stdin);
		if (result == nullptr) {
			break;
		}
		input_string += string {buffer};
	}

	free (buffer);

	picojson::value input_value;
	string error = picojson::parse (input_value, input_string);
	
	if (! error.empty ()) {
		cout << drameater::make_cgi_header (string {"500"});
		cout << "Invalid input " << __LINE__ << endl;
		cout << error << endl;
		exit (0);
	}

	if (! input_value.is <picojson::object> ()) {
		cout << drameater::make_cgi_header (string {"500"});
		cout << "Invalid input " << __LINE__ << endl;
		exit (0);
	}
	auto input_object = input_value.get <picojson::object> ();

	if (input_object.find (string {"scale"}) == input_object.end ()) {
		cout << drameater::make_cgi_header (string {"500"});
		cout << "Invalid input " << __LINE__ << endl;
		exit (0);
	}
	auto scale_value = input_object.at (string {"scale"});
	if (! scale_value.is <string> ()) {
		cout << drameater::make_cgi_header (string {"500"});
		cout << "Invalid input " << __LINE__ << endl;
		exit (0);
	}
	string scale_string = scale_value.get <string> ();

	stringstream scale_stream (scale_string);
	unsigned int scale_number = 0;
	scale_stream >> scale_number;

	if (! (1 <= scale_number && scale_number << 16)) {
		cout << drameater::make_cgi_header (string {"500"});
		cout << "Invalid input " << __LINE__ << endl;
		exit (0);
	}

	string session_id;
	string initializer;
	string start;
	unsigned int step;
	
	drameater::create_quiz (
		scale_number,
		session_id,
		initializer,
		start,
		step
	);

	cout << drameater::make_cgi_header ();
	
	cout
		<< "{"
		<< "\"sessionId\":\"" << session_id << "\","
		<< "\"initializer\":\"" << initializer << "\","
		<< "\"start\":\"" << start << "\","
		<< "\"step\":" << step
		<< "}";

	return 0;
}

