#include <iostream>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include <sstream>
#include <cassert>

#include "picojson.h"
#include "cpplock.h"
#include "strsha256.h"
#include "drameater.h"

using namespace std;


class DeletePostException: public exception {
};


static void delete_post (string a_id)
{
	{
		string filename {"/var/lib/dram-eater-notes/notes.cjson"};

		cpplock::WriteLock write_lock {filename};

		FILE *in = fopen (filename.c_str (), "r");
		if (in == nullptr) {
			throw (DeletePostException {});
		}

		auto buffer_1 = static_cast <char *> (malloc (4096));
		assert (buffer_1 != nullptr);

		string buffer_2;
		
		vector <string> cjson_to_write_back;

		for (; ; ) {
			auto result = fgets (buffer_1, 4096, in);
			if (result == nullptr) {
				break;
			}
			buffer_2 += string {buffer_1};

			if (buffer_2.back () == '\n') {
				bool to_delete = false;

				try {
					picojson::value post_value;
					string error = picojson::parse (post_value, buffer_2);
					if (! error.empty ()) {
						throw (DeletePostException {});
					}
					if (! post_value.is <picojson::object> ()) {
						throw (DeletePostException {});
					}
					auto post_object = post_value.get <picojson::object> ();
					
					if (! (
						post_object.find (string {"id"}) != post_object.end ()
						&& post_object.at (string {"id"}).is <string> ()
					)) {
						throw (DeletePostException {});
					}

					string id_string = post_object.at (string {"id"}).get <string> ();
					
					if (a_id == id_string) {
						to_delete = true;
					}
				} catch (DeletePostException &e) {
					/* Do nothing. */
				}
				
				if (! to_delete) {
					cjson_to_write_back.push_back (buffer_2);
				}
				
				buffer_2.clear ();
			}
		}
			
		free (buffer_1);
		
		fclose (in);
		
		ofstream out (filename);
		for (auto json: cjson_to_write_back) {
			out << json;
		}
	}

	{
		string filename {"/var/lib/dram-eater-notes/locked.cjson"};

		cpplock::WriteLock write_lock {filename};

		FILE *in = fopen (filename.c_str (), "r");
		if (in == nullptr) {
			throw (DeletePostException {});
		}

		auto buffer_1 = static_cast <char *> (malloc (4096));
		assert (buffer_1 != nullptr);

		string buffer_2;
		
		vector <string> cjson_to_write_back;

		for (; ; ) {
			auto result = fgets (buffer_1, 4096, in);
			if (result == nullptr) {
				break;
			}
			buffer_2 += string {buffer_1};

			if (buffer_2.back () == '\n') {
				bool to_delete = false;

				try {
					picojson::value post_value;
					string error = picojson::parse (post_value, buffer_2);
					if (! error.empty ()) {
						throw (DeletePostException {});
					}
					if (! post_value.is <picojson::object> ()) {
						throw (DeletePostException {});
					}
					auto post_object = post_value.get <picojson::object> ();
					
					if (! (
						post_object.find (string {"id"}) != post_object.end ()
						&& post_object.at (string {"id"}).is <string> ()
					)) {
						throw (DeletePostException {});
					}

					string id_string = post_object.at (string {"id"}).get <string> ();
					
					if (a_id == id_string) {
						to_delete = true;
					}
				} catch (DeletePostException &e) {
					/* Do nothing. */
				}
				
				if (! to_delete) {
					cjson_to_write_back.push_back (buffer_2);
				}
				
				buffer_2.clear ();
			}
		}
			
		free (buffer_1);
		
		fclose (in);
		
		ofstream out (filename);
		for (auto json: cjson_to_write_back) {
			out << json;
		}
	}
	
}


int main (int argc, char ** argv)
{
	auto method_c = getenv ("REQUEST_METHOD");
	string method {method_c == nullptr? "": method_c};

	if (method != string {"POST"}) {
		cout << drameater::make_cgi_header (string {"500"});
		cout << "Invalid HTTP method: " << method << endl;
		exit (0);
	}

	auto buffer = static_cast <char *> (malloc (4096));
	assert (buffer != nullptr);

	string input_string;

	for (; ; ) {
		auto result = fgets (buffer, 4096, stdin);
		if (result == nullptr) {
			break;
		}
		input_string += string {buffer};
	}

	free (buffer);

	picojson::value input_value;
	string error = picojson::parse (input_value, input_string);
	
	if (! error.empty ()) {
		cout << drameater::make_cgi_header (string {"500"});
		cout << "Invalid input " << __LINE__ << endl;
		cout << error << endl;
		exit (0);
	}

	if (! input_value.is <picojson::object> ()) {
		cout << drameater::make_cgi_header (string {"500"});
		cout << "Invalid input " << __LINE__ << endl;
		exit (0);
	}
	auto input_object = input_value.get <picojson::object> ();

	if (input_object.find (string {"sessionId"}) == input_object.end ()) {
		cout << drameater::make_cgi_header (string {"500"});
		cout << "Invalid input " << __LINE__ << endl;
		exit (0);
	}
	auto session_id_value = input_object.at (string {"sessionId"});
	if (! session_id_value.is <string> ()) {
		cout << drameater::make_cgi_header (string {"500"});
		cout << "Invalid input " << __LINE__ << endl;
		exit (0);
	}
	string session_id_string = session_id_value.get <string> ();

	if (input_object.find (string {"answer"}) == input_object.end ()) {
		cout << drameater::make_cgi_header (string {"500"});
		cout << "Invalid input " << __LINE__ << endl;
		exit (0);
	}
	auto answer_value = input_object.at (string {"answer"});
	if (! answer_value.is <string> ()) {
		cout << drameater::make_cgi_header (string {"500"});
		cout << "Invalid input " << __LINE__ << endl;
		exit (0);
	}
	string answer_string = answer_value.get <string> ();

	if (input_object.find (string {"id"}) == input_object.end ()) {
		cout << drameater::make_cgi_header (string {"500"});
		cout << "Invalid input " << __LINE__ << endl;
		exit (0);
	}
	auto id_value = input_object.at (string {"id"});
	if (! id_value.is <string> ()) {
		cout << drameater::make_cgi_header (string {"500"});
		cout << "Invalid input " << __LINE__ << endl;
		exit (0);
	}
	string id_string = id_value.get <string> ();

	bool valid = false;
	string inspection;
	try {
		drameater::submit_answer (session_id_string, 1 /* scale */, answer_string, valid, inspection);
	} catch (drameater::DrameaterException &e) {
		cout << drameater::make_cgi_header (string {"500"});
		cout << "DRAM Eater Exception" << endl;
		exit (0);
	}
	
	if (! valid) {
		cout << drameater::make_cgi_header (string {"500"});
		cout << inspection << endl;
		exit (0);
	}

	delete_post (id_string);

	cout << drameater::make_cgi_header ();
	cout << "Fairness is against flattery.";

	return 0;
}

