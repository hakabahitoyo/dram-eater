#include <random>
#include <fstream>
#include <cassert>
#include <sstream>

#include "picojson.h"
#include "cpplock.h"
#include "strsha256.h"
#include "drameater.h"

using namespace std;
using namespace drameater;


static const unsigned int default_step = 0x20000;


void drameater::create_quiz (
	unsigned int scale,
	string &a_session_id,
	string &a_initializer,
	string &a_start,
	unsigned int &a_step
)
{
	string session_id = strsha256::StrRnd256 {} ();
	string initializer = strsha256::StrRnd256 {} ();
	string start = strsha256::StrRnd256 {} ().substr (0, 60);
	
	{
		string filename {"/var/lib/dram-eater-notes/sessions.cjson"};
		cpplock::WriteLock write_lock {filename};
		ofstream out {filename, ofstream::app};
		out
		<< "{"
		<< "\"sessionId\":\"" << session_id << "\","
		<< "\"initializer\":\"" << initializer << "\","
		<< "\"start\":\"" << start << "\","
		<< "\"scale\":\"" << scale << "\""
		<< "}"
		<< endl;
	}
	
	a_session_id = session_id;
	a_initializer = initializer;
	a_start = start;
	a_step = default_step * scale;
}


static string reverse_advance (string a_after_add, string a_instruction)
{
	string after_shift;

	for (unsigned int cn = 0; cn < 15; cn ++) {
		stringstream after_add_piece_stream {a_after_add.substr (cn * 4, 4)};
		unsigned int after_add_piece_number;
		after_add_piece_stream << hex;
		after_add_piece_stream >> after_add_piece_number;

		stringstream instruction_piece_stream {a_instruction.substr (cn * 4 + 4, 4)};
		unsigned int instruction_piece_number;
		instruction_piece_stream << hex;
		instruction_piece_stream >> instruction_piece_number;
		
		unsigned int piece_number
			= (0x10000 + after_add_piece_number - instruction_piece_number) % 0x10000;
		stringstream piece_stream;
		piece_stream << hex;
		piece_stream << nouppercase;
		piece_stream.width (4);
		piece_stream << right;
		piece_stream.fill ('0');
		piece_stream << piece_number;
		
		after_shift += piece_stream.str ();
	}

	stringstream instruction_shift_stream {a_instruction.substr (0, 4)};
	unsigned int shift;
	instruction_shift_stream << hex;
	instruction_shift_stream >> shift;
	shift = shift % 60;

	string input;
	for (unsigned int cn = 0; cn < 60; cn ++) {
		input.push_back (after_shift.at ((shift + cn) % 60));
	}

	return input;
}


void drameater::submit_answer (
	string a_session_id,
	unsigned int a_scale,
	string a_answer,
	bool &a_valid,
	string &a_inspection
)
{
	string initializer;
	string start;
	unsigned int scale = 0;
	bool found = false;

	{
		string filename {"/var/lib/dram-eater-notes/sessions.cjson"};
		cpplock::WriteLock write_lock {filename};

		FILE *in = fopen (filename.c_str (), "r");
		if (in == nullptr) {
			throw (DrameaterException {});
		}

		auto buffer_1 = static_cast <char *> (malloc (4096));
		assert (buffer_1 != nullptr);

		string buffer_2;
		
		vector <string> cjson_to_write_back;

		for (; ; ) {
			auto result = fgets (buffer_1, 4096, in);
			if (result == nullptr) {
				break;
			}
			buffer_2 += string {buffer_1};

			if (buffer_2.back () == '\n') {
				bool to_delete = false;

				try {
					picojson::value session_value;
					string error = picojson::parse (session_value, buffer_2);
					if (! error.empty ()) {
						throw (DrameaterException {});
					}
					if (! session_value.is <picojson::object> ()) {
						throw (DrameaterException {});
					}
					auto session_object = session_value.get <picojson::object> ();
					
					if (! (
						session_object.find (string {"sessionId"}) != session_object.end ()
						&& session_object.at (string {"sessionId"}).is <string> ()
						&& session_object.find (string {"initializer"}) != session_object.end ()
						&& session_object.at (string {"initializer"}).is <string> ()
						&& session_object.find (string {"start"}) != session_object.end ()
						&& session_object.at (string {"start"}).is <string> ()
						&& session_object.find (string {"scale"}) != session_object.end ()
						&& session_object.at (string {"scale"}).is <string> ()
					)) {
						throw (DrameaterException {});
					}
					string session_id_string = session_object.at (string {"sessionId"}).get <string> ();
					string initializer_string = session_object.at (string {"initializer"}).get <string> ();
					string start_string = session_object.at (string {"start"}).get <string> ();
					string scale_string = session_object.at (string {"scale"}).get <string> ();
					
					stringstream scale_stream (scale_string);
					unsigned int scale_number = 0;
					scale_stream >> scale_number;
					
					if (a_session_id == session_id_string) {
						found = true;
						to_delete = true;
						initializer = initializer_string;
						start = start_string;
						scale = scale_number;
					}
				} catch (DrameaterException &e) {
					/* Do nothing. */
				}
				
				if (! to_delete) {
					cjson_to_write_back.push_back (buffer_2);
				}
				
				buffer_2.clear ();
			}
		}
		
		free (buffer_1);
		
		fclose (in);
		
		ofstream out (filename);
		for (auto json: cjson_to_write_back) {
			out << json;
		}
	}

	if (! found) {
		throw (DrameaterException {});
	}

	if (a_scale != scale) {
		a_valid = false;
		stringstream inspection_stream;
		inspection_stream
			<< "Scale must be "
			<< scale
			<< " but get "
			<< a_scale
			<< ".";
		a_inspection = inspection_stream.str ();
		return;
	}

	unsigned int step = default_step * scale;

	string hash = initializer;
	string state = a_answer;
	
	string inspection;
	
	for (unsigned int cn = 0; cn < step; cn ++) {
		// inspection += state + string {" "};
		state = reverse_advance (state, hash);
		// inspection += state + string {"\n"};
		hash = strsha256::StrSha256 {} (hash);
	}

	a_valid = (start == state);
	a_inspection = inspection;
}


string drameater::make_cgi_header (
	string a_status_code,
	string a_content_type
)
{
	return
		string {"Status: "} + a_status_code + string {"\n"}
		+ string {"Content-Type: "} + a_content_type + string {"\n"}
		+ string {"\n"};
}


string drameater::escape_json (string in)
{
	string out;
	for (auto c: in) {
		if (c == '\n') {
			out += string {"\\n"};
		} else if (c == '\t') {
			out += string {"\\t"};
		} else if (c == '"') {
			out += string {"\\\""};
		} else if (c == '\\') {
			out += string {"\\\\"};
		} else if (0x00 <= c && c < 0x20) {
			out += string {"�"};
		} else {
			out.push_back (c);
		}
	}
	return out;
}


