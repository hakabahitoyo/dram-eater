#ifndef DRAMEATER_H
#define DRAMEATER_H


#include <string>


namespace drameater {


using namespace std;


class DrameaterException: public exception {
};


void create_quiz (
	unsigned int scale,
	string &a_session_id,
	string &a_initializer,
	string &a_start,
	unsigned int &a_step
);


void submit_answer (
	string a_session_id,
	unsigned int a_scale,
	string a_answer,
	bool &a_valid,
	string &a_inspection
);


string make_cgi_header (
	string a_status_code = string {"200"},
	string a_content_type = string {"text/plain"}
);


string escape_json (string in);


}; /* namespace drameater */


#endif /* #ifndef DRAMEATER_H */

