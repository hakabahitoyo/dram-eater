#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cassert>

#include "picojson.h"
#include "cpplock.h"
#include "drameater.h"


using namespace std;


void get_locked_posts ()
{
	string filename {"/var/lib/dram-eater-notes/locked.cjson"};

	cpplock::ReadLock read_lock {filename};

	FILE *in = fopen (filename.c_str (), "r");
	assert (in);
	auto buffer = static_cast <char *> (malloc (4096));
	assert (buffer);

	cout << "Content-Type: text/plain" << endl << endl;

	for (; ; ) {
		auto result = fgets (buffer, 4096, in);
		if (result == nullptr) {
			break;
		}
		cout << buffer;
	}

	free (buffer);

	fclose (in);
}


int main (int argc, char ** argv)
{
	auto method_c = getenv ("REQUEST_METHOD");
	string method {method_c == nullptr? "": method_c};

	if (method != string {"POST"}) {
		cout << drameater::make_cgi_header (string {"500"});
		cout << "Invalid HTTP method: " << method << endl;
		exit (0);
	}

	auto buffer = static_cast <char *> (malloc (4096));
	assert (buffer != nullptr);

	string input_string;

	for (; ; ) {
		auto result = fgets (buffer, 4096, stdin);
		if (result == nullptr) {
			break;
		}
		input_string += string {buffer};
	}

	free (buffer);

	picojson::value input_value;
	string error = picojson::parse (input_value, input_string);
	
	if (! error.empty ()) {
		cout << drameater::make_cgi_header (string {"500"});
		cout << "Invalid input " << __LINE__ << endl;
		cout << error << endl;
		exit (0);
	}

	if (! input_value.is <picojson::object> ()) {
		cout << drameater::make_cgi_header (string {"500"});
		cout << "Invalid input " << __LINE__ << endl;
		exit (0);
	}
	auto input_object = input_value.get <picojson::object> ();

	if (input_object.find (string {"sessionId"}) == input_object.end ()) {
		cout << drameater::make_cgi_header (string {"500"});
		cout << "Invalid input " << __LINE__ << endl;
		exit (0);
	}
	auto session_id_value = input_object.at (string {"sessionId"});
	if (! session_id_value.is <string> ()) {
		cout << drameater::make_cgi_header (string {"500"});
		cout << "Invalid input " << __LINE__ << endl;
		exit (0);
	}
	string session_id_string = session_id_value.get <string> ();

	if (input_object.find (string {"answer"}) == input_object.end ()) {
		cout << drameater::make_cgi_header (string {"500"});
		cout << "Invalid input " << __LINE__ << endl;
		exit (0);
	}
	auto answer_value = input_object.at (string {"answer"});
	if (! answer_value.is <string> ()) {
		cout << drameater::make_cgi_header (string {"500"});
		cout << "Invalid input " << __LINE__ << endl;
		exit (0);
	}
	string answer_string = answer_value.get <string> ();

	bool valid = false;
	string inspection;
	try {
		drameater::submit_answer (session_id_string, 4 /* scale */, answer_string, valid, inspection);
	} catch (drameater::DrameaterException &e) {
		cout << drameater::make_cgi_header (string {"500"});
		cout << "DRAM Eater Exception" << endl;
		exit (0);
	}
	
	if (! valid) {
		cout << drameater::make_cgi_header (string {"500"});
		cout << inspection << endl;
		exit (0);
	}

	get_locked_posts ();

	return 0;
}


