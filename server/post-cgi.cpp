#include <iostream>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include <sstream>
#include <cassert>

#include "picojson.h"
#include "cpplock.h"
#include "strsha256.h"
#include "drameater.h"

using namespace std;


static void record_post (string a_text, bool a_locked)
{
	auto now = time (nullptr);
	string id = strsha256::StrRnd256 {} ();

	{
		string filename_public {"/var/lib/dram-eater-notes/notes.cjson"};
		cpplock::WriteLock write_lock {filename_public};
		ofstream out {filename_public, ofstream::app};

		out
		<< "{"
		<< "\"id\":\"" << id << "\","
		<< "\"time\":\"" << now << "\","
		<< "\"text\":\"" << drameater::escape_json (a_locked? string {"🔒"}: a_text) << "\","
		<< "\"scope\":\"" << (a_locked? "Locked": "Public") << "\""
		<< "}"
		<< endl;
	}

	{
		string filename_locked {"/var/lib/dram-eater-notes/locked.cjson"};
		cpplock::WriteLock write_lock {filename_locked};
		ofstream out {filename_locked, ofstream::app};

		out
		<< "{"
		<< "\"id\":\"" << id << "\","
		<< "\"time\":\"" << now << "\","
		<< "\"text\":\"" << drameater::escape_json (a_text) << "\","
		<< "\"scope\":\"" << (a_locked? "Unlocked": "Public") << "\""
		<< "}"
		<< endl;
	}
}


int main (int argc, char ** argv)
{
	auto method_c = getenv ("REQUEST_METHOD");
	string method {method_c == nullptr? "": method_c};

	if (method != string {"POST"}) {
		cout << drameater::make_cgi_header (string {"500"});
		cout << "Invalid HTTP method: " << method << endl;
		exit (0);
	}

	auto buffer = static_cast <char *> (malloc (4096));
	assert (buffer != nullptr);

	string input_string;

	for (; ; ) {
		auto result = fgets (buffer, 4096, stdin);
		if (result == nullptr) {
			break;
		}
		input_string += string {buffer};
	}

	free (buffer);

	picojson::value input_value;
	string error = picojson::parse (input_value, input_string);
	
	if (! error.empty ()) {
		cout << drameater::make_cgi_header (string {"500"});
		cout << "Invalid input " << __LINE__ << endl;
		cout << error << endl;
		exit (0);
	}

	if (! input_value.is <picojson::object> ()) {
		cout << drameater::make_cgi_header (string {"500"});
		cout << "Invalid input " << __LINE__ << endl;
		exit (0);
	}
	auto input_object = input_value.get <picojson::object> ();

	if (input_object.find (string {"sessionId"}) == input_object.end ()) {
		cout << drameater::make_cgi_header (string {"500"});
		cout << "Invalid input " << __LINE__ << endl;
		exit (0);
	}
	auto session_id_value = input_object.at (string {"sessionId"});
	if (! session_id_value.is <string> ()) {
		cout << drameater::make_cgi_header (string {"500"});
		cout << "Invalid input " << __LINE__ << endl;
		exit (0);
	}
	string session_id_string = session_id_value.get <string> ();

	if (input_object.find (string {"answer"}) == input_object.end ()) {
		cout << drameater::make_cgi_header (string {"500"});
		cout << "Invalid input " << __LINE__ << endl;
		exit (0);
	}
	auto answer_value = input_object.at (string {"answer"});
	if (! answer_value.is <string> ()) {
		cout << drameater::make_cgi_header (string {"500"});
		cout << "Invalid input " << __LINE__ << endl;
		exit (0);
	}
	string answer_string = answer_value.get <string> ();

	if (input_object.find (string {"text"}) == input_object.end ()) {
		cout << drameater::make_cgi_header (string {"500"});
		cout << "Invalid input " << __LINE__ << endl;
		exit (0);
	}
	auto text_value = input_object.at (string {"text"});
	if (! text_value.is <string> ()) {
		cout << drameater::make_cgi_header (string {"500"});
		cout << "Invalid input " << __LINE__ << endl;
		exit (0);
	}
	string text_string = text_value.get <string> ();

	if (input_object.find (string {"locked"}) == input_object.end ()) {
		cout << drameater::make_cgi_header (string {"500"});
		cout << "Invalid input " << __LINE__ << endl;
		exit (0);
	}
	bool locked_bool = input_object.at (string {"locked"}).evaluate_as_boolean ();

	bool valid = false;
	string inspection;
	try {
		drameater::submit_answer (session_id_string, 1 /* scale */, answer_string, valid, inspection);
	} catch (drameater::DrameaterException &e) {
		cout << drameater::make_cgi_header (string {"500"});
		cout << "DRAM Eater Exception" << endl;
		exit (0);
	}
	
	if (! valid) {
		cout << drameater::make_cgi_header (string {"500"});
		cout << inspection << endl;
		exit (0);
	}

	record_post (text_string, locked_bool);

	cout << drameater::make_cgi_header ();
	cout << "Fairness is against flattery.";

	return 0;
}

